package dominio;

/** Esta clase define un producto con su precio y cantidad correspondiente 
 * @author Tomas Machin, Alfredo Martinez, Alberto Ruiz, Francisco Rodriguez
 */
public class Comida{
	private int id;
	private String nombre;
	private float precio;
	private int cant;
	/** Para obtener el nombre del producto, el precio  la cantidad de este
	 * @author Nombre de la comida
	 */
	public Comida(){
		this.id = id;
	}

	public Comida(int id, String nombre, float precio, int cant){
		this.id = id;
		this.nombre = nombre;
		this.precio = precio;
		this.cant = cant;
	}
	
	public int getId(){
		return id;
	}
	public void setId(int id){
		this.id = id;
	}

	public String getNombre(){
		return nombre;
	}
	/** Metodo para cambiar el nombre del producto del catalogo
	 * @param nuevo nombre del producto
	 */
	public void setNombre(String nombre){
		this.nombre = nombre;
	}
	public float getPrecio(){
		return precio;
	}
	/** Metodo para cambiar el precio del producto
	 * @param nueva cantidad del precio
	 */
	public void setPrecio(float precio){
		this.precio = precio;
	}
	public int getCant(){
		return cant;
	}
	/** Metodo para cambiar la cantidad querida del producto
	 * @param nueva cantidad del producto 
	 */
	public void setCant(int cant){
		this.cant = cant;
	}
	
	 public boolean equals(Object o){
                if(o == this){
                        return true;
                }
                if(o instanceof Comida){
                        Comida com = (Comida) o;
                        if(com.getId() == id){
                                return true;
                        }
                }
                return false;
        }

	public String toString(){
		String mensaje = "";
		mensaje += "  " + id + " " + nombre + "        " + precio + "        " + cant;
		return mensaje;
	}

}
