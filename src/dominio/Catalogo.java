package dominio;

/** Esta clase define un catalogo de los productos queridos
 * @author Tomas Machin, Alfredo Martinez, Alberto Ruiz, Francisco Rodriguez
 */

import java.util.ArrayList;

public class Catalogo{
	private ArrayList<Comida> comidas = new ArrayList<>();

	 /** Para obtener una lista con los productos deseados
         * @author nombre del array (lista) de la comida
         */

	public ArrayList<Comida> getComidas(){
		return comidas;
	}	
	
	public void setComidas(ArrayList<Comida> comidas){
		this.comidas = comidas;
	}
	
	/** Para añadir un producto a la lista del catalogo
         * @param añadir un producto a la lista
         */

	public void annadirComida(Comida comida){
		comidas.add(comida);
	}
	
	/*public void borrarProducto(Comidas comida){
                comidas.remove(comida);
        }

        private Catalogo modificarCatalogo(){
                Scanner sc = new Scanner(System.in);
                int id, indice;
                System.out.println("Introduzca el id de la línea a modificar");
                indice = sc.nextInt();
                indice = comidas.indexOf(id);
                if(indice != -1){
                        comidas.remove(comida);
                        comidas.add(new Comida(palabras[0], palabras[1], palabras[2], palabras[3], palabras[4]));
                } else {
                        System.out.println("Id no encontrado");
                }
        }*/


	public String toString(){
		StringBuilder mensaje = new StringBuilder();
		if(comidas.size() == 0){
			mensaje.append("El catálogo está vacío");
		} else {
			for(Comida comida: comidas){
				mensaje.append(comida + "\n");
			}
		}
		return mensaje.toString();
	}
}

