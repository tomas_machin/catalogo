package interfaz;

import dominio.*;
import java.io.FileNotFoundException;
import java.io.File;
import java.util.Scanner;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.NumberFormatException;

/** Esta clase define el interfaz del programa
 * @author Tomas Machin, Alberto Ruiz, Alfredo Martinez, Francisco Rodriguez
 */
public class Interfaz{
	private static String NOMBRE_FICHERO = "catalogo.txt";
	/** Define un metodo para separar un array de palabras por espacios
	 * @param Separacion de un array por espacios
	 */
	public static void procesarPeticion(String sentencia){
		String[] palabras = sentencia.split(" ");
		Catalogo catalogo = inicializarCatalogo();
		if(palabras[0].equals("add") && palabras.length == 4){
			Comida comida = new Comida(palabras[1], Float.parseFloat(palabras[2]), Integer.parseInt(palabras[3]));
			System.out.println(comida);
			catalogo.annadirComida(comida);
			guardarCatalogo(catalogo);
		} else if(palabras[0].equals("list")){
			System.out.println("  PRODUCTOS   |  PRECIO  |  CANTIDAD");
			System.out.println("--------------------------------------");
			System.out.println(catalogo);
		} else if(palabras[0].equals("help")){
			printHelp();
		} else if (palabras[0].equals("delete") && palabras.length == 1)<
			Scanner s = new Scanner(System.in);
                        boolean condicion = true;
                        try{
                                while(condicion){
                                        System.out.println("Tiene 3 opciones: [1] borrar producto, [2] editar producto, [3] no hacer nada, marque el que necesite");
                                        int opcion;
                                        condicion = false;
                                        opcion = s.nextInt();
                                        switch (opcion){
                                        	case 1:
                                                	System.out.println("Introduzca el id de la línea que quiere borrar: ");
                                              	  Comida comida = new Comida(Integer.parseInt(palabras[1]));
                                                	//Catalogo.borrarProducto(Comida);
                                                	//guardarCatalogo(catalogo);
                                                	break;
                                                case 2:
                                                	System.out.println(catalogo);
                                                	//modificarCatalogo();
                                              		break;
                                                case 3:
                                                        System.out.println("Vuelva a ejecutar el comando delete para añadir, modificar o borrar algo del catálogo, gracias");
                                                        break;
                                                default:
                                                        condicion = true;
                                        }
                                }
                        }catch (InputMismatchException e){
                                                s.nextInt();
                        } s.close();
                }else {
			printHelp();
		}
	}
	/** Define la lectura del catalogo 
	 * @param lectura del catalogo
	 */
	private static Catalogo inicializarCatalogo(){
		Catalogo catalogo = new Catalogo();
		try{
			File file = new File(NOMBRE_FICHERO);
			Scanner sc = new Scanner(file);
			while(sc.hasNext()){
				String nombre = sc.next();
				String str = sc.next();
				float precio = Float.parseFloat(str);
				String cantidad = sc.next();
				int cant = Integer.parseInt(cantidad);
				Comida comida = new Comida(nombre, precio, cant);
				catalogo.annadirComida(comida);
			}
			sc.close();
		} catch (FileNotFoundException e){}
		return catalogo;
	}
	
	/** Almacena los datos del catalogo
	 *@param datos del catalogo
	 */
	private static void guardarCatalogo(Catalogo catalogo){
		try{
			FileWriter fw = new FileWriter(NOMBRE_FICHERO);
			fw.write(catalogo.toString());
			fw.close();
			System.out.println("Se ha guardado el catálogo con éxito");
		} catch(IOException e){
			System.out.println("Ha habido un error al guardar el fichero");
		}
	}
	/** Expone las ayudas del programa
	 * @param ayudas del programa
	 */
	private static void printHelp(){
		String ayuda = "Las operaciones posibles son las siguientes:\n"+
                        "- Añadir producto:\n" +
                        "\tjava -jar catalogo.jar add <id> <nombre> <precio> <cantidad>\n"+
                        "\tPor ejemplo,\n"+
                        "\t\tjava -jar catalogo.jar add manzana 1 12\n"+
                        "- Mostrar catalogo:\n"+
                        "\tjava -jar catalogo.jar list\n"+
                        "- Eliminar ún producto del catálogo (borrar línea del producto):\n"+
                        "\tjava -jar catalogo.jar delete\n"+
                        "- Mostrar esta ayuda:\n"+
                        "\tjava -jar catalogo.jar help";
			System.out.println(ayuda);
	}
}

