# CATÁLOGO #

### __INTEGRANTES DEL GRUPO:__
#####	- __Tomás Machín__
#####	- __Albeto Ruiz__
#####	- __Alfredo Martinez__	
#####	- __Francisco Rodriguez__
___
## Descripción general
El siguiente repositorio contiene el código para la creación de un Catálogo.
___
## Ejecute el ejemplo de incrustación de la aplicación HTML5 en una ventana de Java Swing

**1.** Descargar una aplicacion para ejecutar la aplicacion. (ej.Git)  
**2.** Clonar este repositorio.  
**3.** Introduzca `make -jar` para generar un fichero jar y poder empezar a utilizar el programa.  
___
* Para utilizar el programa:  
  **1.** Introduzca `java -jar catalogo.jar add <producto> <precio> <cantidad>` para añadir un **objeto** al catalogo.  
  **2.** Introduzca `java -jar catalogo.jar list` para mostrar el **catálogo**.  
  **3.** Introduzca `java -jar catalogo.jar help` para mostrar la **ayuda**.  
___
## Ejemplo
  **1.** `java -jar catalogo.jar add manzanas 1 20`  
  **2.** `java -jar catalogo.jar add 1.5 8`  
  **3.** `java -jar catalogo.jar add 2 14`  
  **4.** `java -jar catalogo.jar list`  

|PRODUCTOS|PRECIO|CANTIDAD|
|:-:|:-:|:-:|
|manzanas|1|20|
|peras|1.5|8|
|platanos|2|14|

___
## Diagrama
![Diagrama UML](https://github.com/bertitahceu/Catalogo/raw/master/Diagramas/class_diagram.png)  
___
## Renuncias
* Este es un ejemplo de inicio.  
* Es posible que el repositorio no se mantenga de forma activa.  
___
## Licencia
MIT  
El código de este repositorio está cubierto por la licencia incluida.  
___
## Apoyo
Ingrese un problema en el repositorio para cualquier pregunta o problema.


<<<<<<< HEAD
![Diagrama UML](C:\Users\machi\workspace\catalogo\Diagramas\class-diagram.jpg "Diagramas")
=======
>>>>>>> 73ca2f3aa0300609d205046be82a3c40aa25da70
